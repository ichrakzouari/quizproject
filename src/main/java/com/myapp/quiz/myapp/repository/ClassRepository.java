package com.myapp.quiz.myapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.myapp.quiz.myapp.models.Class;

public interface ClassRepository extends MongoRepository<Class, String> {
  List<Class> findByName(String name);
}
