package com.myapp.quiz.myapp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.myapp.quiz.myapp.models.Teacher;

public interface TeacherRepository extends MongoRepository<Teacher, String> {

}
