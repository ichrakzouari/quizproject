package com.myapp.quiz.myapp.repository;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.myapp.quiz.myapp.models.ERole;
import com.myapp.quiz.myapp.models.Role;
public interface RoleRepository  extends MongoRepository<Role, String>  {

	  Optional<Role> findByName(ERole name);
	
}
