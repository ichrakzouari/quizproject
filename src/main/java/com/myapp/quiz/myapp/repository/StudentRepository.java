package com.myapp.quiz.myapp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.myapp.quiz.myapp.models.Student;

public interface StudentRepository extends MongoRepository<Student, String> {

}
