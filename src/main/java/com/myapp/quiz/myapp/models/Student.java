package com.myapp.quiz.myapp.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "students")
public class Student {
	
	 @Id
	  private String id;

	  @NotBlank
	  @Size(max = 20)
	  private String username;

	  @NotBlank
	  @Size(max = 50)
	  @Email
	  private String email;

	  @NotBlank
	  @Size(max = 120)
	  private String password;
 
	  @DBRef
	  private Class classes;
	  
	  
	  public Student() {
	  }
	  
	  
	  
	  
	  public Student(String username, String email, String password,Class classes) {
		    this.username = username;
		    this.email = email;
		    this.password = password;
		    this.classes=classes;
		  }

		  public String getId() {
		    return id;
		  }

		  public void setId(String id) {
		    this.id = id;
		  }

		  public String getUsername() {
		    return username;
		  }
		  
		  
		  public void setUsername(String username) {
			    this.username = username;
			  }

			  public String getEmail() {
			    return email;
			  }

			  public void setEmail(String email) {
			    this.email = email;
			  }

			  public String getPassword() {
			    return password;
			  }

			  public void setPassword(String password) {
			    this.password = password;
			  }

			  public Class getClasses() {
			    return classes;
			  }
			  
			  
			  public void setClasses(Class classes) {
				    this.classes = classes;
				  }
	  
	  
	  

	
	
	
	
	
	
	
	
	
	
}
