package com.myapp.quiz.myapp.models;

public enum ERole {
	 ROLE_USER,
	  ROLE_MODERATOR,
	  ROLE_ADMIN
}
