package com.myapp.quiz.myapp.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "teachers")
public class Teacher {

	@Id
	  private String id;

	  @NotBlank
	  @Size(max = 20)
	  private String username;

	  @NotBlank
	  @Size(max = 50)
	  @Email
	  private String email;

	  @NotBlank
	  @Size(max = 120)
	  private String password;

	  @DBRef
	  private Class classe;
	  
	  
	  public Teacher() {
	  }
	  
	  
	  
	  
	  public Teacher (String username, String email, String password,Class classe) {
		    this.username = username;
		    this.email = email;
		    this.password = password;
		    this.classe=classe;
		  }

		  public String getId() {
		    return id;
		  }

		  public void setId(String id) {
		    this.id = id;
		  }

		  public String getUsername() {
		    return username;
		  }
		  
		  
		  public void setUsername(String username) {
			    this.username = username;
			  }

			  public String getEmail() {
			    return email;
			  }

			  public void setEmail(String email) {
			    this.email = email;
			  }

			  public String getPassword() {
			    return password;
			  }

			  public void setPassword(String password) {
			    this.password = password;
			  }

			  public Class getClasse() {
			    return classe;
			  }
			  
			  
			  public void setClasse(Class classe) {
				    this.classe = classe;
				  }
	  
	  
}
