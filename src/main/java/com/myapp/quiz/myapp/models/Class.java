package com.myapp.quiz.myapp.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "classes")
public class Class {
  @Id
  private String id;

  private String name;
;

  public Class() {

  }

  public Class(String name) {
    this.name = name;

  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

 
  @Override
  public String toString() {
    return "class [id=" + id + ", name=" + name + "]";
  }
}