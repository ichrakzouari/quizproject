package com.myapp.quiz.myapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.quiz.myapp.models.Teacher;
import com.myapp.quiz.myapp.repository.TeacherRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class TeacherController {
	@Autowired
	TeacherRepository teacherRepository;
	
	@PostMapping("/teachers")
	
	public ResponseEntity<Teacher> createTeacher(@RequestBody Teacher teacher) {
		try {
			Teacher _teacher = teacherRepository.save(new Teacher(teacher.getUsername(),
					teacher.getEmail(),teacher.getPassword(),teacher.getClasse()));
		    return new ResponseEntity<>(_teacher, HttpStatus.CREATED);
		  } catch (Exception e) {
		    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
		}
	
}
