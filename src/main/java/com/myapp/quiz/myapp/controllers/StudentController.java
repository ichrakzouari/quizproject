package com.myapp.quiz.myapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myapp.quiz.myapp.models.Class;
import com.myapp.quiz.myapp.models.Student;
import com.myapp.quiz.myapp.repository.ClassRepository;
import com.myapp.quiz.myapp.repository.StudentRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class StudentController {
	@Autowired
	StudentRepository studentRepository;
	
	@PostMapping("/students")
	
	public ResponseEntity<Student> createStudent(@RequestBody Student student) {
		try {
		    Student _student = studentRepository.save(new Student(student.getUsername(),
		    		student.getEmail(),student.getPassword(),student.getClasses()));
		    return new ResponseEntity<>(_student, HttpStatus.CREATED);
		  } catch (Exception e) {
		    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
		}
	
}
