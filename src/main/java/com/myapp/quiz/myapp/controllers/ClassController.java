package com.myapp.quiz.myapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import com.myapp.quiz.myapp.models.Class;

import com.myapp.quiz.myapp.repository.ClassRepository;


@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class ClassController {
	@Autowired
	ClassRepository classRepository;
	
	@PostMapping("/classes")
	
	public ResponseEntity<Class> createClass(@RequestBody Class classe) {
		try {
		    Class _classe = classRepository.save(new Class(classe.getName()));
		    return new ResponseEntity<>(_classe, HttpStatus.CREATED);
		  } catch (Exception e) {
		    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
		}
	
	@GetMapping("/classes")
	public ResponseEntity<List<Class>> getAllClasses(@RequestParam(required = false) String name) {
	  try {
	    List<Class> classes = new ArrayList<Class>();

	    if (name== null)
	      classRepository.findAll().forEach(classes::add);
	    else
	      classRepository.findByName(name).forEach(classes::add);

	    if (classes.isEmpty()) {
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    }

	    return new ResponseEntity<>(classes, HttpStatus.OK);
	  } catch (Exception e) {
	    return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	}
}



